'use strict'

const amqp = require('amqplib');
const _ = require('lodash');

function getConnectionUrl(options) {
  // create connection string with options:
  const heartbeat = options.heartbeat;
  let url = options.url;

  if (heartbeat) {
    // url += `?heartbeat=${heartbeat}`;
  }

  sails.log.verbose(`Rabbitmq connection url: ${url}`);

  return url;
}

module.exports = sails => {
  return {
    identity: 'worker',
    configure() {
      if (!_.isObject(sails.config.amqpworker)) {
        sails.config.amqpworker = {
          options: {}
        }
      }
    },
    async initialize(next) {
      const options = sails.config.amqpworker.options

      // default retry connection attempts to 3
      if (!_.has(options, 'retryConnectionAttempts')) {
        options.retryConnectionAttempts = 3
      }

      if (!_.has(options, 'retryDroppedConnectionAttempts')) {
        options.retryDroppedConnectionAttempts = 100
      }

      // default retry connection timeout to 10 seconds
      if (!_.has(options, 'retryConnectionTimeout')) {
        options.retryConnectionTimeout = 10000
      }

      // default retry failed jobs
      if (!_.has(options, 'retryFailedJobs')) {
        options.retryFailedJobs = true
      }

      // Do nothing if not running jobs
      if (!options.runJobs) {
        return next();
      }

      try {
        await connect(options);
      } catch (err) {
        await handleDroppedConnection(err, options);
      }

      next();
    }
  }
}

async function setupChannel(conn, options, jobs) {
  // set up the channel and the delayed job exchange
  const ch = await conn.createChannel();

  sails.amqpworker.channel = ch;

  // log channel level errors
  ch.on('error', err => {
    sails.log.error(err)
  })

  ch.on('close', () => {
    sails.log.verbose('Rabbitmq channel closed')
  })

  // Default to prefetch = 1, can be overridden in job definition
  const prefetchCount = options.prefetchCount === undefined ? 1 : options.prefetchCount;
  ch.prefetch(prefetchCount);

  return new Promise((resolve, reject) => {
    sails.after('hook:orm:loaded', async function() {
      registerWorkers(ch, jobs, options).then(resolve).catch(reject)
    })
  })
}

/**
 * Connect to RabbitMQ server
 *
 * @param {object} options
 */
async function connect(options) {
  const jobs = sails.config.amqpworker.jobs;
  const connectionUrl = getConnectionUrl(options);

  const conn = await amqp.connect(connectionUrl);

  sails.amqpworker = {
    connection: conn
  };

  // log connection errors, try to reconnect
  conn.on('error', err => {
    handleDroppedConnection(err, options)
  })

  return setupChannel(conn, options, jobs)
}

/**
 * Configure the workers defined in the parent project's api/jobs directory to consume the appropriate queues
 *
 * @param {object} channel
 * @param {object[]} jobs
 * @param {string} options
 */
async function registerWorkers(channel, jobs, options) {
  for (let i in jobs) {
    const jobData = jobs[i];

    // Default to a durable queue, can be overridden in job definition
    const durable = !(jobData.durable === false);

    await channel.assertQueue(jobData.queue, { durable });

    // Get wrapped worker function that automatically handles ack/nack
    const ackWorker = createWrappedWorker(channel, jobData, options);

    channel.consume(jobData.queue, ackWorker, {
      noAck: false
    });
  }
}

/**
 * Wrap worker function to include ack/nack automatically (with a single retry)
 *
 * @param {object} channel
 * @param {object} jobData
 * @param {object} options
 */
function createWrappedWorker(channel, jobData, options) {
  return async function(message) {
    try {
      message.body = message.content.toString();

      await jobData.worker(message);

      // channel.ack does not return a promise
      channel.ack(message);
    } catch(err) {
      if (err.message === 'Channel closed') {
        sails.log.error(`Rabbitmq channel closed during job processing for ${message.fields.routingKey}, job will be requeued`);
        return;
      }

      const retry = !message.fields.redelivered && options.retryFailedJobs;
      channel.nack(message, false, retry);
    }
  }
}

/**
 * @param {object} err
 * @param {object} options
 */
async function handleDroppedConnection(err, options) {
  sails.log.error('Connection to rabbitmq dropped');
  options.retryConnectionAttempts = options.retryDroppedConnectionAttempts;

  await handleConnectionError(err, options);
  sails.log.info('Reconnected to rabbitmq');
}

/**
 * @param {object} err
 * @param {object} options
 */
async function handleConnectionError(err, options) {
  const retryAttempts = options.retryConnectionAttempts;
  sails.log.error('Problem connecting to rabbitmq server', err);

  if (retryAttempts > 0) {
    sails.log.info('Attempting to reconnect to rabbitmq server, retry attempts remaining: ', retryAttempts);
    options.retryConnectionAttempts = retryAttempts - 1;

    try {
      await new Promise(resolve => {
        setTimeout(() => {
          resolve(connect(options))
        }, options.retryConnectionTimeout)
      });
    } catch(e) {
      return handleConnectionError(err, options)
    }
  } else {
    sails.log.info('Unable to connect to rabbitmq server')
    throw err;
  }
}
